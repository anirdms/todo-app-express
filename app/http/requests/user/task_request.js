const Request = require('../request')

class TaskRequest extends Request{
    constructor() {
        super({
            title: 'required|string|max:40',
            description: 'required|string'
        })
    }
}

module.exports = new TaskRequest()