const Controller = require('../controller')
const TodoService = require('../../services/user/todo_service')
const taskRequest = require('../../requests/user/task_request')

class TaskController extends Controller {
    /**
     * TodoService constructor.
     */
    constructor () {
        super()
        this.service = new TodoService
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    index = async (request, response) => response.json( await this.service.index(request))

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    store = async (request, response) => response.json( await this.service.store( request))

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    read = async (request, response) => response.json( await this.service.read( request))

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    update = async (request, response) => response.json(await this.service.update(request))

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    delete = async (request, response) => response.json( await this.service.delete( request))
}

module.exports = new TaskController()