const Controller = require('../../controller')
const TodoService = require('../../../services/user/todo_service')
const taskRequest = require('../../../requests/user/task_request')

class TodoController extends Controller {
    /**
     * TodoService constructor.
     */
    constructor () {
        super()
        this.service = new TodoService
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    index = async (request, response) => {
        const serviceResponse = await this.service.index( request)
        if (!serviceResponse.success) return response.redirect('back')
        return this.view('user/todo/index', {data:{tasks: serviceResponse.data},layout: 'user.hbs'}, request, response)
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    create = (request, response) => {
        return this.view('user/todo/create', {layout: 'user.hbs'}, request, response)
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    store = async (request, response) => {
        const serviceResponse = await this.service.store( request)
        return this.webResponse( serviceResponse, '/user/task', {}, request,response)
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    read = async (request, response) => {
        const serviceResponse = await this.service.read( request)
        if (!serviceResponse.success) return response.redirect('back')
        return this.view('user/todo/read', {data:{task: serviceResponse.data},layout: 'user.hbs'}, request, response)
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    edit = async (request, response) => {
        const serviceResponse = await this.service.read( request)
        if (!serviceResponse.success) return response.redirect('back')
        return this.view('user/todo/edit', {data:{task: serviceResponse.data},layout: 'user.hbs'}, request, response)
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    update = async (request, response) => {
        const serviceResponse = await this.service.update( request)
        return this.webResponse( serviceResponse, '/user/task', {}, request,response)
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @return {JSON}
     */
    delete = async (request, response) => {
        const serviceResponse = await this.service.delete( request)
        return this.webResponse( serviceResponse, '/user/task', {}, request,response)
    }
}

module.exports = new TodoController()