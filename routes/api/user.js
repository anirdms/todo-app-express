const express = require('express')
const route = express.Router()

const taskController = require('../../app/http/controllers/api/todo_controller')
const taskRequest = require('../../app/http/requests/user/task_request')
const {authApi}  = require('../../app/http/middlewares/authentication')

//middleware
route.use(authApi)

route.get('/task', taskController.index)
route.post('/task', taskRequest.validators, taskRequest.validate, taskController.store)
route.get('/task/:id', taskController.read)
route.put('/task/update/:id', taskRequest.validators, taskRequest.validate, taskController.update)
route.delete('/task/delete/:id', taskController.delete)

module.exports = route