const express = require('express')
const route = express.Router()

const dashboardController = require('../../app/http/controllers/web/user/dashboard_controller')
const todoController = require('../../app/http/controllers/web/user/todo_controller')
const taskRequest = require('../../app/http/requests/user/task_request')
const {auth}  = require('../../app/http/middlewares/authentication')

//middleware
route.use(auth)

//dashboard
route.get('/dashboard', dashboardController.dashboard)

//task
route.get('/task', todoController.index)
route.get('/task/create', todoController.create)
route.post('/task', taskRequest.validators, taskRequest.validate, todoController.store)
route.get('/task/:id', todoController.read)
route.get('/task/:id/edit', todoController.edit)
route.post('/task/:id/update', taskRequest.validators, taskRequest.validate, todoController.update)
route.get('/task/:id/delete', todoController.delete)

module.exports = route